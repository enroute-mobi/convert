require 'spec_helper'

RSpec.describe Convert::GtfsToNetex::StopDecorator do
  subject(:decorator) { described_class.new(stop) }

  let(:stop) { GTFS::Stop.new }

  describe "#netex_attributes" do
    subject { decorator.netex_attributes }

    context "when GTFS Stop latitude is 48.858093" do
      before { stop.lat = "48.858093" }
      it { is_expected.to include(latitude: "48.858093") }
    end

    context "when GTFS Stop longitude is 2.294694" do
      before { stop.lat = "2.294694" }
      it { is_expected.to include(latitude: "2.294694") }
    end
  end

  describe '#netex_quay?' do
    subject { decorator.netex_quay? }

    context 'when GTFS Stop is stop?' do
      before { allow(stop).to receive(:stop?).and_return(true) }

      it { is_expected.to be_truthy }
    end

    context "when GTFS Stop isn't stop?" do
      before { allow(stop).to receive(:stop?).and_return(false) }

      it { is_expected.to be_falsy }
    end
  end

  describe '#netex_resource_class' do
    subject { decorator.netex_resource_class }

    context 'when netex_quay? is detected' do
      before { allow(decorator).to receive(:netex_quay?).and_return(true) }

      it { is_expected.to eq(Netex::Quay) }
    end

    context "when netex_quay? isn't detected" do
      before { allow(decorator).to receive(:netex_quay?).and_return(false) }

      it { is_expected.to eq(Netex::StopPlace) }
    end
  end

  describe '#parent_station?' do
    subject { decorator.parent_station? }

    context 'when parent_station is nil' do
      before { stop.parent_station = nil }

      it { is_expected.to be_falsy }
    end

    context 'when parent_station is ""' do
      before { stop.parent_station = "" }

      it { is_expected.to be_falsy }
    end

    context 'when parent_station is " "' do
      before { stop.parent_station = " " }

      it { is_expected.to be_falsy }
    end

    context 'when parent_station is "dummy"' do
      before { stop.parent_station = "dummy" }

      it { is_expected.to be_truthy }
    end
  end

   describe '#embedded?' do
    subject { decorator.embedded? }

    context 'when netex_quay? is detected' do
      before { allow(decorator).to receive(:netex_quay?).and_return(true) }

      context 'when parent_station is present' do
        before { allow(decorator).to receive(:parent_station?).and_return(true) }

        it { is_expected.to be_truthy }
      end

      context "when parent_station isn't present" do
        before { allow(decorator).to receive(:parent_station?).and_return(false) }

        it { is_expected.to be_falsy }
      end
    end

    context "when netex_quay? isn't detected" do
      before { allow(decorator).to receive(:netex_quay?).and_return(false) }

      it { is_expected.to be_falsy }
    end
  end

end

RSpec.describe Convert::GtfsToNetex::Services::Decorator do
  subject(:decorator) { described_class.new(service) }

  let(:calendar) { GTFS::Calendar.new }
  let(:service) { GTFS::Service.new(calendar: calendar) }

  def enable_all_days(calendar, except: [])
    (%w{monday tuesday wednesday thursday friday saturday sunday} - Array(except)).each do |day|
      calendar.send "#{day}=", "1"
    end
  end

  describe "#netex_property_of_day" do
    subject { decorator.netex_property_of_day.days_of_week }

    def self.days
      %w{monday tuesday wednesday thursday friday saturday sunday}
    end

    days.each do |day|
      context "when the GTFS Calendar enables the day #{day}" do
        before { calendar.send "#{day}=", "1" }
        it { is_expected.to include(day.capitalize) }
      end
    end

    days.each do |day|
      before { enable_all_days(calendar) }

      context "when the GTFS Calendar disables the day #{day}" do
        before { calendar.send "#{day}=", "0" }
        it { is_expected.to_not include(day.capitalize) }
      end
    end
  end

  describe "#netex_valid_day_bits" do
    subject { decorator.netex_valid_day_bits }

    it { is_expected.to be_nil }

    context "when Calendar date range is 2030-01-01..2030-01-07" do
      before do
        calendar.start_date = '2030-01-01'
        calendar.end_date = '2030-01-07'
      end

      context "and all days are enabled" do
        before do
          enable_all_days(calendar)
        end

        it { is_expected.to eq('1111111') }
      end

      context "all days except monday are enabled" do
        before do
          enable_all_days(calendar, except: 'monday')
        end

        it { is_expected.to eq('1111110') }
      end
    end
  end
end

RSpec.describe Convert::GtfsToNetex::Trips::Decorator do
  subject(:decorator) { described_class.new(trip, stop_times) }

  describe '#netex_base_route_id' do
    subject { decorator.netex_base_route_id }

    context 'when GTFS Trip route_id is AB and stop times stop_ids BEATTY_AIRPORT and BULLFROG' do
      let(:trip) { GTFS::Trip.new(route_id: 'AB') }
      let(:stop_time1) { GTFS::StopTime.new(stop_id: 'BEATTY_AIRPORT') }
      let(:stop_time2) { GTFS::StopTime.new(stop_id: 'BULLFROG') }
      let(:stop_times) { [stop_time1, stop_time2] }

      it { is_expected.to eq('AB-BEATTY_AIRPORT-BULLFROG') }
    end
  end
end

RSpec.describe Convert::GtfsToNetex::Trips::StopTimeDecorator do
  subject(:decorator) { described_class.new(stop_time) }
  let(:stop_time) { GTFS::StopTime.new }

  describe "#netex_passenger_stop_assignment_id" do
    subject { decorator.netex_passenger_stop_assignment_id }

    before do
      stop_time.stop_id = 'stop_id'
      decorator.base_id = 'base_id'
    end

    context 'when StopTime stop-id is "stop_id" and base_id "base_id"' do
      it { is_expected.to eq("base_id-stop_id") }
    end
  end

  describe "#netex_scheduled_stop_point_id" do
    subject { decorator.netex_scheduled_stop_point_id }

    before do
      stop_time.stop_id = 'stop_id'
      decorator.base_id = 'base_id'
    end

    context 'when StopTime stop-id is "stop_id" and base_id "base_id"' do
      it { is_expected.to eq("base_id_stop_id") }
    end
  end

  describe '#netex_passenger_stop_assignment' do
    subject { decorator.netex_passenger_stop_assignment }

    it { is_expected.to have_attributes(order: 1) }
  end
end

RSpec.describe Convert::GtfsToNetex::Agencies::Decorator do
  subject(:decorator) { described_class.new(agency) }

  let(:agency) { GTFS::Agency.new }

  describe '#netex_id' do
    subject { decorator.netex_id }

    context 'when Agency id is defined (e.g. "dummy")' do
      before { agency.id = 'dummy' }

      it { is_expected.to eq('dummy') }
    end

    context 'when Agency is not defined' do
      before { agency.id = nil }

      it { is_expected.to eq('default') }
    end
  end
end
