require 'spec_helper'

RSpec.describe Convert::Base do

  describe '.classify' do

    {
      'none' => 'None',
      'gtfs-netex' => 'GtfsToNetex'
    }.each do |from, to|
      it "returns '#{to}' for '#{from}" do
        expect(Convert.classify(from)).to eq(to)
      end
    end

  end

end
