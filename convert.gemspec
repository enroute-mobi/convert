lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "convert/version"

Gem::Specification.new do |spec|
  spec.name          = "convert"
  spec.version       = Convert::VERSION
  spec.authors       = ["Alban Peignier"]
  spec.email         = ["contact@enroute.mobi"]

  spec.summary       = %q{Convert public transport mode files}
  spec.description   = %q{Convert public transport mode files from a format (GTFS, NeTEx, etc) to another}
  spec.homepage      = "https://chouette-convert.enroute.mobi"
  spec.license       = "Apache License Version 2.0"

  spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://bitbucket.org/enroute-mobi/convert"
  spec.metadata["changelog_uri"] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec', '~> 3.2'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'stackprof'

  spec.add_runtime_dependency 'netex'
  spec.add_runtime_dependency 'gtfs'
  spec.add_runtime_dependency 'neptune'
  spec.add_runtime_dependency 'opentracing'
end
