module Convert
  class NetexToNetex < Base
    def source
      @source ||= Netex::Source.new.tap do |source|
        source.transformers << Netex::Source::Tagger::Line::Indexer.new
      end
    end

    def tagger
      @tagger ||= Netex::Source::Tagger::Line.new source
    end

    def target
      @target ||= Netex::Target.build target_file, profile: netex_profile
    end

    attr_accessor :profile

    def netex_profile
      @netex_profile ||= Netex::Profile.create(profile) if profile
    end

    def convert!
      span('source.read') do
        source.read source_file
      end

      tagger.resources.each do |resource|
        target << resource
      end

      span('target.close') do
        target.close
      end
    end
  end
end
