module Convert
  class GtfsToNetex < Base

    def source
      @source ||= GTFS::Source.build source_file, strict: true
    end

    def target
      @target ||= Netex::Target.build target_file, profile: netex_profile
    end

    attr_accessor :profile

    def netex_profile
      @netex_profile ||= Netex::Profile.create(profile) if profile
    end

    def quay_registry
      @quay_registry ||= QuayRegistry.new
    end

    def convert!
      Stops.new(self).convert
      Stations.new(self).convert
      Trips.new(self).convert

      Routes.new(self).convert
      Agencies.new(self).convert

      services_part = Services.new(self)
      services_part.convert

      min_max_dates = services_part.min_max_dates

      if min_max_dates.present?
        # TODO Use a better ServiceCalendar id
        target <<
          Netex::ServiceCalendar.new(id: '1',
                                     from_date: min_max_dates.min.to_date,
                                     to_date: min_max_dates.max.to_date)
      end

      span('target.close') do
        target.close
      end
    end

    class QuayRegistry

      def quay?(stop_id)
        !quays_index.has_key? stop_id
      end

      def quays_for(parent_station)
        quays_index[parent_station]
      end

      def register(netex_quay, parent_station:)
        quays_index[parent_station] << netex_quay
      end

      protected

      def quays_index
        @quays_index ||= Hash.new { |h,k| h[k] = [] }
      end

    end

    class Part

      def initialize(convert, attributes = {})
        @convert = convert
        attributes.each { |k,v| send "#{k}=", v }
      end

      extend Forwardable
      def_delegators :@convert, :source, :target, :quay_registry

      def self.part_name
        @part_name ||= name.split('::').last.gsub(/([a-z]+)([A-Z])/, '\1_\2').downcase
      end

      def convert
        OpenTracing.start_active_span(self.class.part_name) do
          convert!
        end
      end

    end

    class StopDecorator < SimpleDelegator

      def netex_attributes
        {
          id: id,
          name: name,
          latitude: lat,
          longitude: lon
        }
      end

      def netex_resource
        netex_resource_class.new(netex_attributes)
      end

      def netex_quay?
        stop?
      end

      def netex_resource_class
        netex_quay? ? Netex::Quay : Netex::StopPlace
      end

      def parent_station?
        ! [ nil, "" ].include?(parent_station&.strip)
      end

      def embedded?
        netex_quay? && parent_station?
      end
    end

    class Stops < Part

      def register_quay(gtfs_parent_station, quay)
        quays_index[gtfs_parent_station] << quay
      end

      def convert!
        source.each_stop do |stop|
          next unless stop.stop?

          decorated_stop = StopDecorator.new(stop)

          netex_resource = decorated_stop.netex_resource

          if decorated_stop.embedded?
            quay_registry.register netex_resource,
                                   parent_station: decorated_stop.parent_station
          else
            target << netex_resource
          end
        end
      end

    end

    class Stations < Part

      attr_accessor :quays_index

      def convert!
        source.each_stop do |stop|
          next unless stop.station?

          decorated_stop = StopDecorator.new(stop)

          stop_place = decorated_stop.netex_resource
          stop_place.quays = quay_registry.quays_for(decorated_stop.id)

          target << stop_place
        end
      end

    end

    class Routes < Part

      def convert!
        source.each_route do |route|
          decorated_route = Decorator.new(route)
          target << decorated_route.netex_resource
        end
      end

      class Decorator < SimpleDelegator

        def netex_name
          long_name || short_name
        end

        def netex_attributes
          {
            id: id,
            name: netex_name,
            short_name: short_name
          }
        end

        def netex_tags
          { line_id: id }
        end

        def netex_resource
          Netex::Line.new netex_attributes.merge(tags: netex_tags)
        end

      end

    end

    class Agencies < Part

      def convert!
        source.each_agency do |agency|
          decorated_agency = Decorator.new(agency)
          target << decorated_agency.netex_resource
        end
      end

      class Decorator < SimpleDelegator

        def netex_attributes
          {
            id: netex_id,
            name: name
          }
        end

        def netex_id
          id || "default"
        end

        def netex_resource
          Netex::Operator.new netex_attributes
        end

      end

    end


    class MinMax

      attr_reader :min, :max

      def submit(*values)
        values.each do |value|
          next if value.nil?

          if value.is_a?(Range)
            submit value.min, value.max
            next
          end

          submit_min value
          submit_max value
        end
      end

      def submit_min(value)
        if min.nil? || min > value
          @min = value
        end
      end

      def submit_max(value)
        if max.nil? || max < value
          @max = value
        end
      end

      def present?
        min && max
      end

      def range
        Range.new(min..max) if present?
      end

      def merge!(other)
        submit(other.min, other.max)
      end

    end

    class Services < Part

      def convert!
        source.services.each do |service|
          decorated_service = Decorator.new(service)

          min_max_dates.merge! decorated_service.min_max_dates

          decorated_service.netex_resources.each do |netex_resource|
            target << netex_resource
          end
        end
      end

      def min_max_dates
        @min_max ||= MinMax.new
      end

      class Decorator < SimpleDelegator

        # TODO Check if the timezone introduced by this tranformation is problematic
        def netex_time_range
          if date_range&.min && date_range&.max
            Range.new date_range.min.to_time, date_range.max.to_time
          end
        end

        def netex_valid_day_bits
          to_timetable_period&.to_days_bit&.bitset&.to_s
        end

        def days_of_week
          Cuckoo::Timetable::DaysOfWeek.new.tap do |days_of_week|
            DAYS.each { |day| days_of_week.enable(day) if send("#{day}?") }
          end
        end

        def to_timetable_period
          return nil unless date_range
          Cuckoo::Timetable::Period.new(date_range.min, date_range.max, days_of_week)
        end

        def netex_day_type_id
          service_id
        end

        def netex_day_type
          Netex::DayType.new(id: netex_day_type_id).tap do |day_type|
            properties = [netex_property_of_day].compact
            day_type.properties = properties unless properties.empty?
          end
        end

        DAYS = %w{monday tuesday wednesday thursday friday saturday sunday}
        def netex_property_of_day
          days_of_week = DAYS.map { |day| day.capitalize if send("#{day}?") }.compact.join(' ')
          Netex::PropertyOfDay.new(days_of_week: days_of_week) unless days_of_week.empty?
        end

        def netex_day_type_assignment_id(order)
          "#{service_id}-#{order}"
        end

        def next_assignment_order
          @assignment_order ||= 0
          @assignment_order += 1
        end

        def netex_day_type_ref
          @netex_day_type_ref ||= Netex::Reference.new(netex_day_type_id, type: 'DayType')
        end

        def netex_period_day_type_assignment
          next_day_type_assignment do |assignment|
            assignment.operating_period_ref = Netex::Reference.new(netex_operating_period_id, type: 'OperatingPeriod')
          end
        end

        def next_day_type_assignment(&block)
          Netex::DayTypeAssignment.new.tap do |assignment|
            assignment.order = next_assignment_order
            assignment.id = netex_day_type_assignment_id(assignment.order)

            assignment.day_type_ref = netex_day_type_ref

            block.call assignment
          end
        end

        def netex_operating_period_id
          service_id
        end

        def netex_operating_period
          Netex::UicOperatingPeriod.new(id: netex_operating_period_id).tap do |period|
            period.time_range = netex_time_range
            period.valid_day_bits = netex_valid_day_bits
          end if netex_time_range
        end

        def netex_dates_day_type_assignments
          calendar_dates.map do |calendar_date|
            next_day_type_assignment do |assignment|
              assignment.date = calendar_date.ruby_date
              assignment.is_available = calendar_date.included?
            end
          end
        end

        def netex_resources
          [ netex_day_type ].tap do |resources|
            if period = netex_operating_period
              resources << period
              resources << netex_period_day_type_assignment
            end

            resources.concat netex_dates_day_type_assignments
          end
        end

        def min_max_dates
          MinMax.new.tap do |min_max|
            min_max.submit date_range

            calendar_dates.map do |calendar_date|
              min_max.submit calendar_date.ruby_date
            end
          end
        end

      end
    end

    class Uniqueness

      def resource_ids_by_class
        @resource_ids_by_class ||= Hash.new { |h,k| h[k] = Set.new }
      end

      def resource_ids(resource_class)
        resource_ids_by_class[resource_class.name]
      end

      def exist?(resource_class, resource_id)
        resource_ids(resource_class).include? resource_id
      end

      def add(resource)
        resource_ids(resource.class).add resource.id
      end

      def create?(resource_class, resource_id)
        if !exist?(resource_class, resource_id)
          resource = yield
          add resource
        end
      end

      def uniq?(resource)
        result = resource_ids(resource.class).add?(resource.id)
        !result.nil?
      end

    end

    class TargetWithUniqueness

      def uniqueness
        @uniq_resources ||= Uniqueness.new
      end

      def initialize(target)
        @target = target
      end
      attr_reader :target

      def add(*resources)
        resources.flatten.each do |resource|
          if uniqueness.uniq?(resource)
            target << resource
          end
        end
      end
      alias << add

    end

    class TargetWithTags < SimpleDelegator

      def initialize(target, tags = nil)
        super target
        @tags = tags
      end

      attr_accessor :tags

      def add(*resources)
        resources.flatten.each do |resource|
          resource.tags.merge! tags
        end unless tags.empty?

        super(*resources)
      end
      alias << add

    end

    class Trips < Part

      def uniq_target
        @target_with_uniqueness ||= TargetWithUniqueness.new(target)
      end

      def convert!
        source.each_trip_with_stop_times do |trip, stop_times|
          decorated_trip = Decorator.new(trip, stop_times)

          trip_target = TargetWithTags.new(uniq_target, decorated_trip.netex_tags)

          uniq_target.uniqueness.create?(Netex::Route, decorated_trip.netex_route_id) do
            trip_target << decorated_trip.netex_route_points
            trip_target << decorated_trip.netex_route

            decorated_trip.netex_route
          end
          uniq_target.uniqueness.create?(Netex::ServiceJourneyPattern,
                                         decorated_trip.netex_journey_pattern_id) do
            trip_target << decorated_trip.netex_scheduled_stop_points
            trip_target << decorated_trip.netex_passenger_stop_assignments

            trip_target << decorated_trip.netex_journey_pattern

            decorated_trip.netex_journey_pattern
          end

          # VehicleJourney should be uniq
          trip_target << decorated_trip.netex_vehicle_journey
        end
      end

      class StopTimeDecorator < SimpleDelegator

        def initialize(stop_time, base_id: nil, sequence_offset: 0, quay_registry: nil)
          super stop_time
          @base_id = base_id
          @sequence_offset = sequence_offset
          @quay_registry = quay_registry
        end
        attr_accessor :base_id, :sequence_offset, :quay_registry

        def netex_quay?
          quay_registry && quay_registry.quay?(stop_id)
        end

        def netex_passenger_stop_assignment_id
          "#{base_id}-#{stop_id}"
        end

        def netex_scheduled_stop_point_id
          "#{base_id}_#{stop_id}"
        end

        def netex_stop_id
          stop_id
        end

        def netex_scheduled_stop_point
          Netex::ScheduledStopPoint.new id: netex_scheduled_stop_point_id
        end

        def netex_scheduled_stop_point_ref
          @netex_scheduled_stop_point_ref ||=
            Netex::Reference.new netex_scheduled_stop_point_id,
                                 type: 'ScheduledStopPoint'
        end

        def netex_point_projection
          Netex::PointProjection.new.tap do |projection|
            projection.id = netex_id
            projection.project_to_point_ref = netex_scheduled_stop_point_ref
          end
        end

        def netex_route_point
          Netex::RoutePoint.new.tap do |point|
            point.id = netex_id
            point.projections = [ netex_point_projection ]
          end
        end

        def netex_id
          "#{base_id}-#{stop_sequence}"
        end

        def netex_stop_point_in_journey_pattern_id
          netex_id
        end

        def netex_stop_sequence
          if sequence_offset > 0
            stop_sequence.to_i + sequence_offset
          else
            stop_sequence
          end
        end

        def netex_stop_point_in_journey_pattern
          Netex::StopPointInJourneyPattern.new.tap do |point|
            point.id = netex_stop_point_in_journey_pattern_id
            point.order = netex_stop_sequence

            point.scheduled_stop_point_ref = netex_scheduled_stop_point_ref
          end
        end

        def netex_stop_point_in_journey_pattern_ref
          Netex::Reference.new netex_stop_point_in_journey_pattern_id,
                               type: 'StopPointInJourneyPattern'
        end

        def quay_ref
          Netex::Reference.new netex_stop_id, type: 'Quay' if netex_quay?
        end
        def stop_place_ref
          Netex::Reference.new netex_stop_id, type: 'StopPlace' unless netex_quay?
        end

        def netex_passenger_stop_assignment
          Netex::PassengerStopAssignment.new.tap do |assignment|
            assignment.id = netex_passenger_stop_assignment_id
            assignment.scheduled_stop_point_ref = netex_scheduled_stop_point_ref
            assignment.quay_ref = quay_ref
            assignment.stop_place_ref = stop_place_ref
            assignment.order = 1
          end
        end

        def netex_timetabled_passing_time
          Netex::TimetabledPassingTime.new.tap do |passing_time|
            passing_time.stop_point_in_journey_pattern_ref =
              netex_stop_point_in_journey_pattern_ref

            passing_time.departure_time = netex_departure_time
            if netex_departure_time != netex_arrival_time
              passing_time.arrival_time = netex_arrival_time
            end
            if gtfs_departure_time && gtfs_departure_time.day_offset > 0
              passing_time.departure_day_offset = gtfs_departure_time.day_offset
            end
            if gtfs_arrival_time && gtfs_arrival_time.day_offset > 0
              passing_time.arrival_day_offset = gtfs_arrival_time.day_offset
            end
          end
        end

        def gtfs_departure_time
          @departure_time ||= GTFS::Time.parse(departure_time)
        end

        def gtfs_arrival_time
          @arrival_time ||= GTFS::Time.parse(arrival_time)
        end

        def netex_departure_time
          return unless time = gtfs_departure_time
          Netex::Time.new time.hour, time.minute, time.second
        end

        def netex_arrival_time
          return unless time = gtfs_arrival_time
          Netex::Time.new time.hour, time.minute, time.second
        end

      end

      class Decorator < SimpleDelegator

        attr_reader :stop_times
        def initialize(trip, stop_times = [])
          super trip
          @stop_times = stop_times
        end

        def netex_tags
          @netex_tags ||= { line_id: netex_line_id }
        end

        def netex_line_id
          route_id
        end

        def netex_route
          Netex::Route.new(id: netex_route_id).tap do |route|
            route.line_ref = Netex::Reference.new(netex_line_id, type: 'Line')
          end
        end

        def netex_base_route_id
          @netex_base_route_id ||= [route_id, stop_times.map(&:stop_id)].join('-')
        end

        def netex_route_id
          @netex_route_id ||= "Route:#{netex_base_route_id}"
        end

        def netex_journey_pattern_id
          @netex_journey_pattern_id ||= "ServiceJourneyPattern:#{netex_base_route_id}"
        end

        def netex_vehicle_journey_id
          id
        end

        def netex_sequence_offset
          @netex_sequence_offset ||= stop_times.first&.stop_sequence == "0" ? 1 : 0
        end

        def decorated_stop_times
          @decorated_stop_times ||= stop_times.map do |stop_time|
            StopTimeDecorator.new stop_time, base_id: netex_base_route_id, sequence_offset: netex_sequence_offset
          end
        end

        def netex_scheduled_stop_points
          decorated_stop_times.map(&:netex_scheduled_stop_point)
        end

        def netex_stop_point_in_journey_patterns
          decorated_stop_times.map(&:netex_stop_point_in_journey_pattern)
        end

        def netex_passenger_stop_assignments
          decorated_stop_times.map(&:netex_passenger_stop_assignment)
        end

        def netex_route_points
          decorated_stop_times.map(&:netex_route_point)
        end

        def netex_timetabled_passing_times
          decorated_stop_times.map(&:netex_timetabled_passing_time)
        end

        def netex_journey_pattern
          Netex::ServiceJourneyPattern.new(id: netex_journey_pattern_id).tap do |journey_pattern|
            journey_pattern.route_ref = Netex::Reference.new(netex_route_id, type: 'Route')
            journey_pattern.points_in_sequence = netex_stop_point_in_journey_patterns
          end
        end

        def netex_journey_pattern_ref
          Netex::Reference.new(netex_journey_pattern_id, type: 'JourneyPattern')
        end

        def netex_vehicle_journey
          Netex::ServiceJourney.new.tap do |vehicle_journey|
            vehicle_journey.id = netex_vehicle_journey_id
            vehicle_journey.day_types = [ netex_day_type_ref ]
            vehicle_journey.journey_pattern_ref = netex_journey_pattern_ref

            vehicle_journey.passing_times = netex_timetabled_passing_times
          end
        end

        def netex_day_type_id
          service_id
        end

        def netex_day_type_ref
          Netex::Reference.new(netex_day_type_id, type: 'DayType')
        end

      end

    end

  end
end
