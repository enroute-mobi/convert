module Convert

  def self.create(type, source_file, target_file, options = {})
    class_for(type).new source_file, target_file, options
  end

  def self.classify(type)
    type.to_s.gsub('-','_to_').split('_').collect(&:capitalize).join
  end

  def self.class_for(type)
    const_get classify(type)
  end

  module Spanner

    def span(name)
      OpenTracing.start_active_span(name) do
        yield
      end
    end

  end

  class Base
    include Spanner

    def self.parameterized_type
      @parameterized_type ||= name.gsub(/([a-z]+)([A-Z0-9])/, '\1_\2').downcase
    end

    def initialize(source_file, target_file, options = {})
      options.each { |k,v| send "#{k}=", v }
      @source_file, @target_file = source_file, target_file
    end

    attr_reader :source_file, :target_file

    def convert
      span("convert/#{self.class.parameterized_type}") do
        convert!
      end
    end

  end

  class None < Base

    def convert
      FileUtils.cp source_file, target_file
    end

  end

end
