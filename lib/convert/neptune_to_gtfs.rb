# coding: utf-8
module Convert
  class NeptuneToGtfs < Base

    def source
      @source ||= Neptune::Source.new
    end

    def target
      @target ||= GTFS::Target.new target_file
    end

    def service_ids
      @service_ids ||= {}
    end

    def convert!
      OpenTracing.start_active_span('source.read') do
        source.read source_file
      end

      Companies.new(self).convert
      Lines.new(self).convert
      Timetables.new(self).convert
      VehicleJourneys.new(self).convert
      StopPoints.new(self).convert

      OpenTracing.start_active_span('target.close') do
        target.close
      end
    end

    def stop_point_registry
      @stop_point_registry ||= StopPointRegistry.new
    end

    class StopPointRegistry

      def registered?(stop_point_id)
        stop_point_identifiers.include? stop_point_id
      end

      def register(stop_point_id)
        stop_point_identifiers.add stop_point_id
      end

      protected

      def stop_point_identifiers
        @stop_point_identifiers ||= SortedSet.new
      end

    end

    def service_registry
      @service_registry ||= ServiceRegistry.new
    end

    class ServiceRegistry

      def register(vehicle_journey_ids, service_ids)
        vehicle_journey_ids.each do |vehicle_journey_id|
          service_ids_for_vehicle_journey_id[vehicle_journey_id] = service_ids
        end
      end

      def service_ids(vehicle_journey_id)
        service_ids_for_vehicle_journey_id[vehicle_journey_id]
      end

      protected

      def service_ids_for_vehicle_journey_id
        @service_ids_for_vehicle_journey_id ||= {}
      end

    end

    class Part
      include Spanner

      def initialize(convert, attributes = {})
        @convert = convert
        attributes.each { |k,v| send "#{k}=", v }
      end

      extend Forwardable
      def_delegators :@convert, :source, :target, :stop_point_registry, :service_registry

      def self.part_name
        # FIXME to be shared ()into Spanner ?)
        @part_name ||= name.split('::').last.gsub(/([a-z]+)([A-Z])/, '\1_\2').downcase
      end

      def convert
        span(self.class.part_name) do
          convert!
        end
      end

    end

    class Companies < Part

      def convert!
        source.companies.each do |company|
          decorated_company = Decorator.new(company)
          target.agencies << decorated_company.gtfs_resource
        end
      end

      class Decorator < SimpleDelegator

        def gtfs_attributes
          {
            id: id,
            name: name,
            timezone: gtfs_timezone
          }
        end

        DEFAULT_GTFS_TIMEZONE = "Europe/Paris"
        def gtfs_timezone
          DEFAULT_GTFS_TIMEZONE
        end

        def gtfs_resource
          GTFS::Agency.new gtfs_attributes
        end

      end

    end

    class Lines < Part

      def convert!
        source.lines.each do |line|
          decorated_line = Decorator.new(line)
          target.routes << decorated_line.gtfs_resource
        end
      end

      class Decorator < SimpleDelegator

        def gtfs_short_name
          number
        end

        def gtfs_long_name
          published_name || name
        end

        def self.gtfs_route_types
          @route_types ||= {
            'Air' => 1100,
            'Train' => 2,
            'LongDistanceTrain' => 2,
            'LocalTrain' => 2,
            'RapidTransit' => 2,
            'Metro' => 1,
            'Tramway' => 0,
            'Coach' => 200,
            'Bus' => 3,
            'Ferry' => 4,
            'Waterborne' => 4,
            'Trolleybus' => 3,
            'Shuttle' => 2,
            'Taxi' => 1500,
            'VAL' => 2,
          }
        end

        def gtfs_route_type
          self.class.gtfs_route_types[transport_mode_name]
        end

        def gtfs_attributes
          {
            id: id,
            agency_id: company_id,
            short_name: gtfs_short_name,
            long_name: gtfs_long_name,
            type: gtfs_route_type
          }
        end

        def gtfs_resource
          GTFS::Route.new gtfs_attributes
        end

      end

    end

    class Timetables < Part

      def convert!
        source.timetables.each do |timetable|
          decorated_timetable = TimetableDecorator.new(timetable)

          decorated_timetable.calendars.each { |c| target.calendars << c }
          decorated_timetable.calendar_dates.each { |cd| target.calendar_dates << cd }

          service_registry.register decorated_timetable.vehicle_journey_ids,
                                    decorated_timetable.service_ids
        end
      end

      class TimetableDecorator < SimpleDelegator

        WEEKDAYS = %w{monday tuesday wednesday thursday friday saturday sunday}.freeze

        def lowercase_day_types
          @lowercase_day_types ||= day_types.map(&:downcase)
        end

        def gtfs_days
          @gtfs_days ||= WEEKDAYS.each_with_object({}).each do |day, gtfs_days|
            gtfs_days[day] = lowercase_day_types.include?(day) ? 1 : 0
          end
        end

        def service_ids
          @service_ids ||= Set.new
        end

        def with_service_id(attributes, service_id)
          service_ids << service_id
          attributes.merge service_id: service_id
        end

        def calendars
          decorated_periods.map do |decorated_period|
            with_service_id decorated_period.calendar_attributes.merge(gtfs_days),
                            period_service_id(decorated_period)
          end
        end

        def calendar_dates
          decorated_dates.map do |decorated_date|
            with_service_id decorated_date.calendar_date_attributes, default_service_id
          end
        end

        def decorated_dates
          @decorated_dates ||= calendar_days.map do |calendar_day|
            DateDecorator.new calendar_day
          end
        end

        def decorated_periods
          @decorated_periods ||= periods.each_with_index.map do |period, index|
            PeriodDecorator.new(period, index)
          end
        end

        def default_service_id
          id
        end

        def period_service_id(decorated_period)
          if decorated_period.first?
            default_service_id
          else
            "#{default_service_id}-#{decorated_period.index}"
          end
        end

      end

      class PeriodDecorator < SimpleDelegator

        def initialize(period, index)
          @index = index
          super period
        end

        attr_reader :index

        def first?
          index == 0
        end

        def calendar_start_date
          start_of_period.strftime('%Y%m%d')
        end

        def calendar_end_date
          end_of_period.strftime('%Y%m%d')
        end

        def calendar_attributes
          {
            start_date: calendar_start_date,
            end_date: calendar_end_date
          }
        end

      end

      class DateDecorator < SimpleDelegator
        def calendar_date_date
          strftime('%Y%m%d')
        end

        def calendar_date_exception_type
          1
        end

        def calendar_date_attributes
          {
            date: calendar_date_date,
            exception_type: calendar_date_exception_type
          }
        end
      end

    end

    class VehicleJourneys < Part

      def convert!
        source.vehicle_journeys.each do |vehicle_journey|
          decorated_vehicle_journey = Decorator.new vehicle_journey

          service_ids = service_registry.service_ids(decorated_vehicle_journey.id)
          if service_ids.nil?
            # VehicleJourney without Timetable
            puts decorated_vehicle_journey.id
            next
          end

          service_ids.each do |service_id|
            gtfs_trip = decorated_vehicle_journey.gtfs_resource(service_id)
            target.trips << gtfs_trip

            vehicle_journey.stops.each_with_index do |stop, position|
              decorated_stop = AtStopDecorator.new stop, position: position
              target.stop_times << decorated_stop.gtfs_resource(gtfs_trip.id)
            end
          end
        end
      end

      class Decorator < SimpleDelegator

        def gtfs_trip_id(service_id)
          "#{id}-#{service_id}"
        end

        def gtfs_trip_headsign
          published_journey_name
        end

        def gtfs_route_id
          line_id
        end

        def gtfs_attributes(service_id)
          {
            id: gtfs_trip_id(service_id),
            route_id: gtfs_route_id,
            trip_headsign: gtfs_trip_headsign,
            service_id: service_id,
            # direction_id: direction_id,
          }
        end

        def gtfs_resource(service_id)
          GTFS::Trip.new gtfs_attributes(service_id)
        end

      end

      class AtStopDecorator < SimpleDelegator

        def initialize(vehicle_journey_at_stop, position: nil)
          super vehicle_journey_at_stop
          @position = position
        end

        attr_reader :position

        def gtfs_attributes(trip_id)
          {
            trip_id: trip_id,
            departure_time: departure_time,
            arrival_time: arrival_time,
            stop_id: stop_point_id,
            stop_sequence: position
          }
        end

        def gtfs_resource(trip_id)
          GTFS::StopTime.new gtfs_attributes(trip_id)
        end

      end

    end

    class StopPoints < Part

      def convert!
        source.stop_points.each do |stop_point|
          decorated_stop_point = Decorator.new(stop_point)
          target.stops << decorated_stop_point.gtfs_resource
        end
      end

      class Decorator < SimpleDelegator

        def gtfs_attributes
          {
            id: id,
            name: name,
            lat: latitude,
            lon: longitude
          }
        end

        def gtfs_resource
          GTFS::Stop.new gtfs_attributes
        end

      end

    end

  end
end
