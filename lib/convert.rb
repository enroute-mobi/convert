require "convert/version"

module Convert
  class Error < StandardError; end
  # Your code goes here...
end

require 'opentracing'
require 'netex'
require 'gtfs'
require 'neptune'
require 'cuckoo'
require 'forwardable'

require "convert/base"
require "convert/gtfs_to_netex"
require "convert/neptune_to_gtfs"
require "convert/netex_to_netex"
